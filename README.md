``` bash
# install dependencies
yarn install

# start json-server for Ajax 
json-server --id T src/server/data.json

# serve with hot reload at localhost
yarn serve

```