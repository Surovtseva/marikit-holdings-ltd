export function groupBy(items, key){
  let groupedItems = {};
  for(let i = 0; i < items.length; i++){
    let itemsKey = items[i][key];
    if(!groupedItems[itemsKey]){
      groupedItems[itemsKey] = [];
    }
    groupedItems[itemsKey].push(items[i]);
  }
  return groupedItems;
}