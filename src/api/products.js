import axios from 'axios';
import {groupBy} from '../services/products';

export function getProducts(){
  let products = {};
  return axios.get('http://localhost:3000/products')
    .then((response) => {
      products = groupBy(response.data, "G");
      return products;
    })
    .catch(e => {
      return e;
    });
}

export function putProduct(data){
  return axios.put('http://localhost:3000/products/' + data.T, data)
    .then((response) => {
      return true;
    })
    .catch(e => {
      return false;
    });
}